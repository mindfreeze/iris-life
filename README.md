# iris

**iris** is a new project aimed to be royalty-free real-time video streaming solution focused on speed and low-latency.

The canonical repositaory URL for this repo is https://gitlab.com/vibhoothiiaanand/iris

The projects landing page: https://mindfreeze.videolan.me/iris/

This project is done as Research Project which is supported by Xiph, Mozilla, VideoLAN, Google, FFMpeg, and MultiCoreware Inc under the championship of Nathan Egge from Mozilla.

## Goal and Features

The goal of this project is to provide a very fast real time video streaming solution for Power Constrained devices by doing a complextiy analysis of next generation Video Codec **AV1**'s rust implementation **rav1e** in a power constrained device and acheive the highest speed possible so we can have high quality and smooth real-time video transmision with low latency and for a given Device.

The project's main goal is to have NEON SIMD functions for *rav1e*'s encoding functions which will acceralate encoding speeds.

Another goal of the project is to mesure and comparing the battery-life of *dav1d* decoder with hardware decoder (H.264) and see whether the software implementation is good enough
It's currently in very early research state.

In future, this project will have a simple cli tool for doing everything.

# Roadmap

The plan is the following 

### Reached  
1.  Identifying main goals.
2.  Project's roadmap for next one year.

### On-going
1. Build system devleopment.

### After
1.  Have full NEON SIMD for rav1e for ARMv8 ( AArch64) 
2.  Complexity Analysis of AV1 Encoders (including rav1e) on ARMv8 using RD-Tool.
2.  Measuring Battery-life of dav1d full decoding with hardware H.264.




## License 

This project is going to be relased with a very liberal licesning with "BSD 2-Clause License" so people can use it anywhere.


## CLA 

There is no CLA.

People will keep their copyright and their authorship rights, while adhering to the BSD 2-clause license.

Amrita Viswha Vidyapeetham will only have the collective work rights.

## CoC
Please Refer to [Code of Conduct](CODE_OF_CONDUCT.md) document for more information

## Contributing


## Getting in Touch 
Join us on the IRC channel #iris on Freenode! If you don't have IRC set up you can easily connect from your [web browser](https://webchat.freenode.net/?channels=%23iris).

## FAQ

Please check [here](FAQ.md)

## The Team

*  Dr. Jayaraj Pooror   - Project Mentor
*  Vibhoothi            - Research Assistant    
*  Krishnan Iyer        - Research Assistant
*  Vaishnav Sivadas     - Research Assistant    

Other Notable people from Video Technology 
* Dr. Nathan Egge, Senior Research Engineer at Mozilla and also Co-author of the AV1, is helping with development of rav1e.